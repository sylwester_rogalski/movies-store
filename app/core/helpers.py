from django.conf import settings
from omdb import OMDB


def get_movie_from_omdb(title):

    omdb = OMDB(api_key=getattr(settings, "OMBD_API_KEY"))
    data = omdb.get(title=title)

    return {k.lower(): v for k, v in data.items()}  # lower keys in dict