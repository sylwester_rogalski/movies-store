from django.db.models import Count
from django.forms import model_to_dict
from django.utils.dateparse import parse_datetime
from rest_framework import status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from core import helpers
from core.api.serializers import MovieSerializer, CommentSerializer, TopMoviesSerializer, MovieTitleSerializer
from core.exceptions import QueryParametersParsingException
from core.models import Movie, Comment


class MovieListCreateAPIView(APIView):

    def get(self, request):
        movies = Movie.objects.filter()
        serializer = MovieSerializer(movies, many=True)
        return Response(serializer.data)

    def post(self, request):

        serializer = MovieTitleSerializer(data=request.data)
        if serializer.is_valid():
            title = serializer.data.get("title")

            movie = Movie.objects.filter(title=title).first()

            if movie is None:
                movie_data = helpers.get_movie_from_omdb(title)
                serializer = MovieSerializer(data=movie_data)

                if serializer.is_valid():
                    serializer.save()

                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    data = {"error": {"message": "Movie can not be added to database", "errors": serializer.errors},
                            "data": serializer.data}

                    return Response(data, status=status.HTTP_424_FAILED_DEPENDENCY)

            else:
                return Response(model_to_dict(movie), status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MovieDetailAPIView(APIView):

    def get_object(self, pk):
        article = get_object_or_404(Movie, pk=pk)
        return article

    def get(self, request, pk):
        movie = self.get_object(pk)
        serializer = MovieSerializer(movie)
        return Response(serializer.data)


class CommentListCreateAPIView(APIView):

    def get(self, request):
        movie_id = request.query_params.get('movie_id', None)

        if movie_id is None:
            comments = Comment.objects.filter()
        else:
            if not movie_id.isdigit(): raise QueryParametersParsingException("movie_id parameter should be a number")

            comments = Comment.objects.filter(movie__id=movie_id)

        serializer = CommentSerializer(comments, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CommentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TopMoviesListAPIView(APIView):

    def get(self, request):

        from_date = request.query_params.get('from_date', None)
        to_date = request.query_params.get('to_date', None)

        self.__class__.validate_params(from_date, to_date)

        queryset = Movie.filter_by_dates(from_date, to_date)

        movies = queryset.annotate(Count("comments")).order_by("-comments__count")

        serializer = TopMoviesSerializer(movies, many=True, from_date=from_date, to_date=to_date)
        return Response(serializer.data)

    @staticmethod
    def validate_params(*datetimes):

        for datetime in datetimes:
            if datetime is not None:
                if parse_datetime(datetime) is None:
                    raise QueryParametersParsingException("Incorrect datetime format")
