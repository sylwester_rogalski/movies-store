from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from core.models import Movie, Comment


class MovieTitleSerializer(serializers.Serializer):
    title = serializers.CharField(required=True)

    def validate(self, attrs):
        if len(self.initial_data) > 1:
            raise ValidationError({"error": "Request body should contain only 'title' parameter"})
        else:
            return super().validate(attrs)


class MovieSerializer(serializers.ModelSerializer):
    released = serializers.DateField(input_formats=['%d %b %Y', ])

    class Meta:
        model = Movie
        fields = "__all__"

        extra_kwargs = {
            'id': {'read_only': True},
            'created_at': {'read_only': True},
        }


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = "__all__"

        extra_kwargs = {
            'id': {'read_only': True},
        }


class TopMoviesSerializer(serializers.Serializer):

    def __init__(self, *args, **kwargs):
        self.from_date = kwargs.pop('from_date', None)
        self.to_date = kwargs.pop('to_date', None)
        super().__init__(self, *args, **kwargs)

    movie_id = serializers.IntegerField(read_only=True, source="id")
    total_comments = serializers.SerializerMethodField()
    rank = serializers.SerializerMethodField()

    def get_total_comments(self, object):
        return object.total_comments(from_date=self.from_date, to_date=self.to_date)

    def get_rank(self, object):
        ranking = Movie.generate_ranking(from_date=self.from_date, to_date=self.to_date)

        count_of_comments = object.total_comments(from_date=self.from_date, to_date=self.to_date)

        return ranking[count_of_comments]
