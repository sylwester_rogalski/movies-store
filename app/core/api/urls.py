from django.urls import path

from core.api.views import MovieListCreateAPIView, MovieDetailAPIView, CommentListCreateAPIView, TopMoviesListAPIView

urlpatterns = [
    path("movies/",
         MovieListCreateAPIView.as_view(),
         name="movie-list"),

    path("movies/<int:pk>/",
         MovieDetailAPIView.as_view(),
         name="movie-detail"),

    path("comments/",
         CommentListCreateAPIView.as_view(),
         name="comment-list"),

    path("top/",
         TopMoviesListAPIView.as_view(),
         name="movies-top"),
]

