class QueryParametersParsingException(Exception):

    def __init__(self, msg=None):
        if msg is None:
            msg = "Some query parameters has incorrect types"

        error = '{"message": "%s", "error_type": "Error occurred during parsing query parameter"}' % msg
        super().__init__(error)
