from django.db import models
from django.db.models import Sum, Count


class Movie(models.Model):
    """
    Movie model : model for Movies
    """
    title = models.CharField(max_length=500, unique=True)
    year = models.IntegerField(null=True, blank=True)
    released = models.DateField(null=True, blank=True)
    runtime = models.CharField(max_length=20, default="N/A")
    director = models.CharField(max_length=500, default="N/A")
    actors = models.CharField(max_length=1000, default="N/A")
    plot = models.TextField(default="N/A")
    type = models.CharField(max_length=20, default="N/A")
    writer = models.TextField(default="N/A")

    created_at = models.DateTimeField(auto_now_add=True)

    def total_comments(self, from_date=None, to_date=None):
        queryset = Comment.objects

        if from_date is not None: queryset = queryset.filter(created_at__gte=from_date)
        if to_date is not None: queryset = queryset.filter(created_at__lte=to_date)

        return queryset.filter(movie__id=self.id).count()

    @staticmethod
    def generate_ranking(from_date=None, to_date=None):

        queryset = Movie.filter_by_dates(from_date, to_date)

        count_comments = queryset.annotate(Count("comments")).values_list('comments__count', flat=True) \
            .distinct().order_by("-comments__count")

        return {count_comments[i]: i + 1 for i in range(0, len(count_comments))}

    @staticmethod
    def filter_by_dates(from_date, to_date):

        queryset = Movie.objects

        if from_date is not None and to_date is not None:
            queryset = queryset.filter(comments__created_at__range=[from_date, to_date])
        else:
            if from_date is not None: queryset = queryset.filter(comments__created_at__gte=from_date)
            if to_date is not None: queryset = queryset.filter(comments__created_at__lte=to_date)

        return queryset

    class Meta:
        verbose_name = "Movie"
        verbose_name_plural = "Movies"

    def __str__(self):
        return self.title


class Comment(models.Model):
    """
    Movie model : model for Movies
    """
    content = models.TextField(null=False, blank=False)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE, related_name="comments")

    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Comment"
        verbose_name_plural = "Comments"

    def __str__(self):
        return self.content
