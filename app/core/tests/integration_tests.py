from datetime import datetime
from unittest.mock import patch

from django.db.models.functions import datetime
from rest_framework import status
from rest_framework.test import APITestCase


class EnpointsTestCase(APITestCase):
    request_movie_data = {"title": "Love Is in the Air"}
    title = "Love Is in the Air"

    # helper function, should be realised in other way
    def create_movie(self, year=1999):
        released = datetime.datetime.now().date()

        response_data = {"title": self.title, "year": str(year), "released": released}

        with patch('core.helpers.get_movie_from_omdb') as mock:
            mock.return_value = response_data

            return self.client.post("/api/movies/", self.request_movie_data)

    def create_comment(self, movie_id, comment = "TEST COMMENT"):
        request_comment_data = {"movie": movie_id, "content": comment}
        return self.client.post("/api/comments/", request_comment_data)

    def test_post_movie(self):
        year = 2000
        response = self.create_movie(year=year)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)  # check correct status code
        self.assertEqual(response.data['id'], 1)  # check if added to db
        self.assertEqual(response.data['year'], year)

    # should be rewriten to use initial data
    def test_get_movies(self):
        self.create_movie()
        response = self.client.get("/api/movies/", self.request_movie_data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)  # check correct status code
        self.assertEqual(len(response.data), 1)  # check correct status code

    # should be rewriten to use initial data
    def test_post_comment(self):
        test_comment = "sample comment"

        response = self.create_movie()
        response = self.create_comment(response.data["id"], comment=test_comment)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)  # check correct status code
        self.assertEqual(response.data['content'], test_comment)  # check correct status code
        self.assertEqual(response.data['id'], 1)

    # duplication of code because lack of time
    def test_get_comments(self):

        response = self.create_movie()
        self.create_comment(response.data["id"])
        self.create_comment(response.data["id"])

        response = self.client.get("/api/comments/", self.request_movie_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)  # check correct status code
        self.assertEqual(len(response.data), 2)

    # duplication of code because lack of time
    def test_get_top(self):

        response = self.create_movie()
        self.create_comment(response.data["id"])
        self.create_comment(response.data["id"])

        response = self.client.get("/api/top/", self.request_movie_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)  # check correct status code
        self.assertEqual(len(response.data), 2)