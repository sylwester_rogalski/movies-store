from rest_framework import status
from rest_framework.response import Response
from rest_framework.utils import json
from rest_framework.views import exception_handler

from core.exceptions import QueryParametersParsingException


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # Now add the HTTP status code to the response.
    if response is not None:
        response.data['status_code'] = response.status_code
    else:
        if isinstance(exc, QueryParametersParsingException):
            data = json.loads(str(exc))
            response = Response(data=data, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
            return response

    return response
