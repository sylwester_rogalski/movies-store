#This is Movies Store App

## Code challange instructions

We'd like you to build simple REST API for us - a basic movie database interacting with external API. Here's full specification of endpoints that we'd like it to have:

`POST /movies:`
Request body should contain only movie title, and its presence should be validated.

Based on passed title, other movie details should be fetched from http://www.omdbapi.com/ (or other similar, public movie database) - and saved to application database.
Request response should include full movie object, along with all data fetched from external API.

`GET /movies:`

Should fetch list of all movies already present in application database.
Additional filtering, sorting is fully optional - but some implementation is a bonus.

`POST /comments:`

Request body should contain ID of movie already present in database, and comment text body.
Comment should be saved to application database and returned in request response.

`GET /comments:`

Should fetch list of all comments present in application database.
Should allow filtering comments by associated movie, by passing its ID.

`GET /top:`

​Should return top movies already present in the database ranking based on a number of comments added to the movie (as in the example) in the specified date range.
The response should include the ID of the movie, position in rank and total number of comments (in the specified date range).
Movies with the same number of comments should have the same position in the ranking.
Should require specifying a date range for which statistics should be generated.

Example response:
```
[

    {

        "movie_id": 2,

        "total_comments": 4,

        "rank": 1

    },

    {

        "movie_id": 3,

        "total_comments": 2,

        "rank": 2

    },

    {

        "movie_id": 4,

        "total_comments": 2,

        "rank": 2

    },

    {

        "movie_id": 1,

        "total_comments": 0,

        "rank": 3

    }

]
```

## Requirements
App was written and tested using Python 3.7 and Django 2.2

In order to install dependencies you need to run following command in the root directory of the project
`pip install -r requirements.txt`

## Start development server

In order to start development server use following commands:

*go into subdirectory*

`cd app` 

*and run development server*

`python manage.py runserver`

*now you can open web browser*

`http://localhost:8000`

## Endpoints short preview

### GET movies

`GET /movies`

*sample response body*
```
[
    {
        "id": 1,
        "released": "2013-04-03",
        "title": "Love Is in the Air",
        "year": 2013,
        "runtime": "96 min",
        "director": "Alexandre Castagnetti",
        "actors": "Ludivine Sagnier, Nicolas Bedos, Jonathan Cohen, Arnaud Ducret",
        "plot": "Antoine is a lawyer living in New York. On his way back to France for the final round of a job interview, Antoine finds himself sitting right next to his ex-girlfriend Julie. With a ...",
        "type": "movie",
        "writer": "Vincent Angell (scenario), Xavier Nemo (adaptation), Julien Simonet (adaptation), Nirina Ralanto (adaptation), Brigitte Bémol (adaptation), Alexandre Castagnetti (adaptation), Nicolas Bedos (adaptation)",
        "created_at": "2019-05-24T19:48:29.170307+02:00"
    }
]
```

### POST movies

`POST /movies`

*sample request body*
```
{
 "title": "Love Is in the Air"
}
```

### GET comments
`GET /comments`

*sample response body*
```
[
    {
        "id": 1,
        "content": "Hello :)",
        "created_at": "2019-05-25T21:32:35.819870+02:00",
        "movie": 1
    },
    {
        "id": 2,
        "content": "Hello 2 :)",
        "created_at": "2019-05-25T21:32:42.990046+02:00",
        "movie": 2
    }
]
```

in order to filter comments by movie use query parameter
`/comments/?movie_id=2`


### POST comments
`POST /comments`

*sample request body*
```
    {
        "content": "Hello hello :)",
        "movie": "2"
    }
```

### GET top commented movies
`GET /top`

*sample response body*
```
[
    {
        "movie_id": 2,
        "total_comments": 2,
        "rank": 1
    },
    {
        "movie_id": 1,
        "total_comments": 1,
        "rank": 2
    }
]
```

in order to filter response by dates use eg.
`/top/?from_date=2019-05-25%2010:59&to_date=2019-05-25%2020:59`


## SUMMARY
If you have any proposition or question write me

sylwester@rogalski.it


## DEPLOYED APP

Deployed app can be found on https://movies-store-app-sample.herokuapp.com/movies/
